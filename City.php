<?php

require_once 'DBConnectionTrait.php';
class City
{
    use DBConnectionTrait;
    protected $district;
    protected $name;
    protected $population;
    protected $countryCode;
    protected $id;

    public function __construct(string $district, string $name, $population, string $countryCode)
    {
        self::loadDatabase();
        $this->district = $district;
        $this->name = $name;
        $this->population = $population;
        $this->countryCode = $countryCode;
    }

    public static function findByCountry(string $countryCode)
    {
        self::loadDatabase();
        $query = self::$pdo->prepare('SELECT id, district, name, population, country_code FROM city where country_code = :country_code');
        $query->execute(['country_code' => $countryCode]);

        return $query->fetchAll();
    }

    public function save()
    {
        if (empty($this->id)) {
            $query = self::$pdo->prepare('INSERT INTO city(district, name, population, country_code) VALUES (:district, :name, :population, :country_code)');
            $result = $query->execute([
                'country_code' => $this->countryCode,
                'district' => $this->district,
                'name' => $this->name,
                'population' => $this->population,
            ]);

            if ($result) {
                $insertedId = self::$pdo->lastInsertId();
                $this->id = $insertedId;
            }
        } else {
            $query = self::$pdo->prepare('UPDATE city SET district=:district, name=:name, population=:population, country_code=:country_code WHERE id=:id');
            $result = $query->execute([
                'country_code' => $this->countryCode,
                'district' => $this->district,
                'name' => $this->name,
                'population' => $this->population,
                'id' => $this->id
            ]);
        }

        return $result;
    }

    public function getCountryCode()
    {
        return $this->countryCode;
    }

    public function getPopulation()
    {
        return $this->population;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getDIstrict()
    {
        return $this->district;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }
}
